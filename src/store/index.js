'use strict';

import Vue from 'vue';
import Vuex from 'vuex';
import moduleList from './moduleList';

Vue.use(Vuex);

const isDebug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: moduleList,
    strict: isDebug,
    plugins: isDebug
        ? [
            // Debug plugins for development environment
        ]
        : [
            // Production plugins for production environment
        ],
});
