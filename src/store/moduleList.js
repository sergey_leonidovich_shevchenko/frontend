'use strict';

import SnackbarModule from '@/components/Snackbar/Snackbar.module';
import AuthenticationModule from '@/components/Security/Authentication/Authentication.module';
import BrokerModule from '@/components/content/pages/Broker/Broker.module';
import IdentityModule from '@/components/Security/Identity/Identity.module';

export default {
    authentication: new AuthenticationModule(),
    broker: new BrokerModule(),
    identity: new IdentityModule(),
    snackbar: new SnackbarModule(),
};
