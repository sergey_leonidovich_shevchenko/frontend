'use strict';

class SecurityHandlerService {
    /**
     * Обработчик ответа от сервера при успешной авторизации
     * @param {{id: number, username: string, email: string, roles: Array<string>}|null} user
     * @param {string|null} token
     * @returns {{user: {any}, token: string}}
     */
    static handlerLoginSuccess({user = null, token = null}) {
        if (!token) {
            throw new Error('С сервера не пришел токен!');
        }

        if (!user) {
            throw new Error('С сервера не пришла информация о авторизованном пользователе!');
        }

        return {user, token};
    }

    /**
     * Обработчик ответа от сервера при не успешной авторизации
     * @param {{any}} response
     * @return {{any}} response
     */
    static handlerLoginError(response) {
        throw new Error(response.message);
    }
}
/**
 * Обработчик сохранения токена
 * @private
 * @param {string} token
 * @returns {string|null} Токен авторизации или null
 */

export default SecurityHandlerService;
