'use strict';

class ResponseHandlerService {
    static STATUS_400_BAD_REQUEST = 400;
    static STATUS_401_UNAUTORIZED = 401;
    static STATUS_403_ACCESS_DENIED = 403;
    static STATUS_404_NOT_FOUND = 404;
    static STATUS_500_BAD_REQUEST = 500;
    static STATUS_503_SERVICE_UNAVAILABLE = 503;
    static STATUS_504_GATEWAY_TIMEOUT = 504;

    /**
     * Хэндлер ответов от сервера
     * @param {{status: number, data: {message: string}}} response Ответ от сервера
     * @returns {{}|undefined} Ответ от сервера
     */
    static handle(response) {
        if (!response) {
            throw new Error('Не могу обработать ответ пришедший с сервера!');
        }

        switch (response.status) {
            case this.STATUS_400_BAD_REQUEST:
                this._handler400();
                break;

            case this.STATUS_401_UNAUTORIZED:
                this._handler401(response);
                break;

            case this.STATUS_403_ACCESS_DENIED:
                this._handler403();
                break;

            case this.STATUS_404_NOT_FOUND:
                this._handler404();
                break;

            case this.STATUS_500_BAD_REQUEST:
                this._handler500();
                break;

            case this.STATUS_503_SERVICE_UNAVAILABLE:
                this._handler503();
                break;

            case this.STATUS_504_GATEWAY_TIMEOUT:
                this._handler504();
                break;
        }

        return response;
    }

    /**
     * Обработка ошибки 400 (ошибка в запросе)
     * @private
     */
    static _handler400() {
        let text = 'Ошибка в запросе!';
        throw new Error(text);
    }

    /**
     * Обработка ошибки 401 (ошибка авторизации)
     * @param {string} message
     * @private
     */
    static _handler401({data: {message}}) {
        let text = 'Ошибка авторизации!';

        switch (message) {
            case 'Invalid credentials.':
                text += ' Не правильные связка логин и/или пароль.';
                break;

            case 'Invalid JWT Token':
                text += ' Невалидный токен авторизации. Авторизуйтесь заново!';
                break;
        }

        throw new Error(text);
    }

    /**
     * Обработка ошибки 403 (доступ к выполняемому действию запрещен)
     * @private
     */
    static _handler403() {
        let text = 'У Вас нет прав на выполняемое действие!';
        throw new Error(text);
    }

    /**
     * Обработка ошибки 404 (Запрашиваемый ресурс не был найден)
     * @private
     */
    static _handler404() {
        let text = 'Запрашиваемый ресурс не был найден!';
        throw new Error(text);
    }

    /**
     * Обработка ошибки 500 (На сервере произошла ошибка)
     * @private
     */
    static _handler500() {
        let text = 'На сервере произошла ошибка!';
        throw new Error(text);
    }

    /**
     * Обработка ошибки 503 (Сервер временно не имеет возможности обрабатывать запросы по техническим причинам)
     * @private
     */
    static _handler503() {
        let text = 'Сервер временно не имеет возможности обрабатывать запросы по техническим причинам (обслуживание, перегрузка и прочее).';
        throw new Error(text);
    }

    /**
     * Обработка ошибки 504 (Таймаут при ожидании ответа от сервера)
     * @private
     */
    static _handler504() {
        let text = 'Сервер в роли шлюза или прокси-сервера не дождался ответа от вышестоящего сервера для завершения текущего запроса.';
        throw new Error(text);
    }
}

export default ResponseHandlerService;
