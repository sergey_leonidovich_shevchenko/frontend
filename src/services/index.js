/**
 * Индексный файл служб группирует все экспорты служб вместе,
 * чтобы их можно было импортировать в другие части приложения,
 * используя только путь к папке, и позволяет импортировать несколько служб в одном выражении
 * (например, import { service1, service2, ... } from './_services').
 */
export * from '@/services/user.service';
export * from '@/components/content/pages/Broker/Broker.service';
