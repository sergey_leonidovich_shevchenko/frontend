'use strict';

import axios from 'axios';
import ResponseHandlerService from '@/services/Handler/ResponseHandler.service';

class RequestService {
    METHOD_GET = 'GET';
    METHOD_POST = 'POST';
    METHOD_PUT = 'PUT';
    METHOD_DELETE = 'DELETE';

    /**
     * Отсылка синхронного запроса на бэкенд
     * @param {string} url
     * @param {string} method
     * @param {{}} data
     * @returns {Promise<*>}
     */
    send(url, method = this.METHOD_GET, data = {}) {
        return axios(this.getRequestOptions(url, method, data))
            .catch(response => {
                ResponseHandlerService.handle(response);
                throw new Error('Произошла ошибка во время запроса!');
            });
    }

    /**
     * Отсылка асинхронного запроса на бэкенд
     * @param {string} url
     * @param {string} method
     * @param {{}} data
     * @returns {{}}
     */
    async sendAsync(url, method = this.METHOD_GET, data = {}) {
        return await axios(this.getRequestOptions(url, method, data))
            .then((({data}) => data))
            .catch(({response}) => {
                throw ResponseHandlerService.handle(response);
            });
    }

    /**
     * Получить опции для формирования запроса
     * @param {string} url
     * @param {string} method
     * @param {{}} data
     * @returns {{url: string, method: string, data: {}, headers: {}, responseType: string}}
     */
    getRequestOptions(url, method = this.METHOD_GET, data = {}) {
        return {
            url,
            method,
            data,
            headers: this._getHeaders(),
            responseType: 'json',
        };
    }

    /**
     * Получить заголовки для отправки на сервер
     * @return {{}}
     * @private
     */
    _getHeaders() {
        let headers = {
            'Content-Type': 'application/json',
        };

        this._addDisabledCORS(headers);
        this._addJWTToken(headers);

        return headers;
    }

    /**
     * Добавить заголовки для отключения CORS
     * @param {{}} headers
     * @private
     */
    _addDisabledCORS(headers) {
        headers['Access-Control-Allow-Origin'] = '*';
        headers['Access-Control-Allow-Methods'] = '*';
        headers['Access-Control-Allow-Headers'] = '*';
        headers['Access-Control-Max-Age'] = '86401';
    }

    /**
     * Добавить JWT токен в заголовки
     * @param {{}} headers
     * @private
     */
    _addJWTToken(headers) {
        let token = localStorage.getItem('authentication-token');
        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }
    }
}

export default RequestService;
