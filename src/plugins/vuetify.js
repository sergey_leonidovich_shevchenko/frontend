'use strict';

import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import VSnackbarQueue from '@tozd/vue-snackbar-queue';

Vue.use(Vuetify);
Vue.use(VSnackbarQueue);

export default new Vuetify({
    theme: {
        dark: true,
    },
    icons: {
        iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    },
});
