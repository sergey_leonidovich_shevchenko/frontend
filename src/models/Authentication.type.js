'use strict';

import IdentityType from '@/components/Security/Identity/Identity.type';

/**
 * Сущность "Аутентификация пользователя в системе"
 */
class Authentication {
    /** @type {boolean} */
    _isAuth = false;

    /** @type {IdentityType|null} */
    _identity = null;

    /** @type {string} */
    _token = '';

    /**
     * @constructor
     * @param {IdentityType} identity
     * @param {boolean|null} isAuth
     * @param token
     */
    constructor({identity = null, isAuth = null, token = null}) {
        this.isAuth = isAuth || false;
        this._identity = identity || new IdentityType({});
        this._token = token || '';
    }

    /**
     * @returns {boolean}
     */
    get isAuth() {
        return this._isAuth;
    }
    /**
     * @param {boolean} isAuth
     */
    set isAuth(isAuth) {
        this._isAuth = isAuth;
    }

    /**
     * @returns {IdentityType|null}
     */
    get identity() {
        return this._identity;
    }
    /**
     * @param {IdentityType|null} identity
     */
    set identity(identity) {
        this._identity = identity;
    }

    /**
     * @returns {string}
     */
    get token() {
        return this._token;
    }
    /**
     * @param {string} token
     */
    set token(token) {
        this._token = token;
    }
}

export default Authentication;
