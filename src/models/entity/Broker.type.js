'use strict';

import moment from 'moment';

class Broker {
    /** @type {number|null}*/
    _id = null;

    /** @type {string|null} */
    _name = null;

    /** @type {Array<{key: string, value: *}>|[]} */
    _setting = [];

    /** @type {moment|null} */
    _createdAt = null;

    /** @type {moment|null} */
    _updatedAt = null;

    /**
     * @param {number|null} id
     * @param {string|null} name
     * @param {Array<{key: string, value: *}>|[]} setting
     * @param {string|null} created_at
     * @param {string|null} updated_at
     */
    constructor({id = null, name = null, setting = [], created_at = null, updated_at = null}) {
        this._id = id;
        this._name = name;
        this._setting = setting;
        this._createdAt = created_at ? moment(created_at): null;
        this._updatedAt = updated_at ? moment(updated_at): null;
    }

    /**
     * @returns {number|null}
     */
    get id() {
        return this._id;
    }

    /**
     * @param {number|null} id
     */
    set id(id) {
        this._id = id;
    }

    /**
     * @returns {string|null}
     */
    get name() {
        return this._name;
    }

    /**
     * @param {string} name
     */
    set name(name) {
        this._name = name;
    }

    /**
     * @returns {Array<{key: string, value: *}>|[]}
     */
    get setting() {
        return this._setting;
    }

    /**
     * @param {Array<{key: string, value: *}>|[]} setting
     */
    set setting(setting) {
        this._setting = setting;
    }

    /**
     * @returns {moment|null}
     */
    get createdAt() {
        return this._createdAt;
    }

    /**
     * @param {moment|null} createdAt
     */
    set createdAt(createdAt) {
        this._createdAt = createdAt;
    }

    /**
     * @returns {moment|null}
     */
    get updatedAt() {
        return this._updatedAt;
    }

    /**
     * @param {moment|null} updatedAt
     */
    set updatedAt(updatedAt) {
        this._updatedAt = updatedAt;
    }
}

export default Broker;
