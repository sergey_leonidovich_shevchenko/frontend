/**
 * Получить значение переменной Env (без префикса "VUE_APP_")
 * @param path
 * @returns {string}
 */
const configEnv = path => {
    return process.env['VUE_APP_' + path];
};
export default configEnv;
