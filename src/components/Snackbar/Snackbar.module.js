'use strict';

/**
 * Модуль оповещения vuex отвечает за snackbar раздел централизованного хранилища состояний,
 * он содержит действия и мутации для установки сообщения об успешном
 * или ошибочном оповещении и для очистки оповещения.
 * В этом модуле каждое действие оповещения просто фиксирует одну мутацию,
 * чтобы можно было зафиксировать мутации непосредственно из ваших компонентов vue и избавиться от действий.
 * Однако я предпочитаю отправлять действия отовсюду для согласованности,
 * а не отправлять действия для одних вещей и совершать мутации для других.
 * Этот способ также обеспечивает немного большую гибкость, если вы решили расширить действие,
 * чтобы сделать больше, чем просто зафиксировать мутацию.
 */
class Snackbar {
    actions = {
        show({commit}, snackbar) {
            commit('SHOW_SNACKBAR', snackbar);
        },

        hide({commit}) {
            commit('HIDE_SNACKBAR');
        },
    };

    getters = {
        /**
         * @param {SnackbarType} snackbar
         * @returns {SnackbarType}
         */
        snackbar: ({snackbar}) => snackbar,
    };

    mutations = {
        /**
         * Показать снэкбар
         * @param state
         * @param {SnackbarType} snackbar
         * @constructor
         */
        SHOW_SNACKBAR: (state, snackbar) => {
            state.snackbar = snackbar;
        },

        /**
         * Скрыть снэкбар
         * @param state
         * @constructor
         */
        HIDE_SNACKBAR: (state) => {
            state.snackbar = null;
        },
    };

    namespaced = true;

    state = {
        snackbar: null,
    };
}

export default Snackbar;
