'use strict';

import configEnv from '@/configEnv';

export const COLOR_ERROR = '#800';
export const COLOR_INFO = '#008';
export const COLOR_SUCCESS = '#080';
export const COLOR_WARNING = '#880';

class SnackbarType {
    _closable = null;
    _color = null;
    _text = null;
    _timeout = null;

    /**
     * @constructor
     * @param {string}       text     Текст сообщения
     * @param {string|null}  color    Цвет snackbar
     * @param {number|null}  timeout  Время автозакрытия (null = нет автозакрытия)
     * @param {boolean|null} closable Может ли пользователь закрыть его (true = у каждого snackbar будет кнопка закрытия)
     */
    constructor({text, color = null, timeout = null, closable = null}) {
        this._closable = closable || true;
        this._color = color || COLOR_INFO;
        this._text = text;
        this._timeout = timeout || configEnv('SNACKBAR_TIMEOUT');
    }

    get closable() {
        return this._closable;
    }

    set closable(closable) {
        this._closable = closable;
    }

    get color() {
        return this._color;
    }

    set color(color) {
        this._color = color;
    }

    get text() {
        return this._text;
    }

    set text(text) {
        this._text = text;
    }

    get timeout() {
        return this._timeout;
    }

    set timeout(timeout) {
        this._timeout = timeout;
    }
}

export default SnackbarType;
