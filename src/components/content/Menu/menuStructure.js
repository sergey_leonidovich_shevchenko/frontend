'use strict';

import {
    mdiAccountBadge,
    mdiAccountCash,
    mdiAccountChildOutline,
    mdiAccountPlus,
    mdiAccountStar,
    mdiAlienOutline,
    mdiCashMultiple,
    mdiCashUsdOutline,
    mdiCheckOutline,
    mdiDatabaseExport,
    mdiDomain,
    mdiHome,
    mdiHomeAccount,
    mdiLogin, mdiLogout,
    mdiStarFace,
} from '@mdi/js';

const entityMenu = [
    {
        name: 'api.account',
        title: 'Account',
        icon: mdiAccountCash,
        visible: ['auth'],
    },
    {
        name: 'api.broker',
        title: 'Broker',
        icon: mdiAccountStar,
        visible: ['auth'],
    },
    {
        name: 'api.consulting_company',
        title: 'ConsultingCompany',
        icon: mdiDomain,
        visible: ['auth'],
    },
    {
        name: 'api.personal_area',
        title: 'PersonalArea',
        icon: mdiAccountBadge,
        visible: ['auth'],
    },
    {
        name: 'api.sales_department',
        title: 'SalesDepartment',
        icon: mdiCashUsdOutline,
        visible: ['auth'],
    },
    {
        name: 'api.sb_check_status',
        title: 'SbCheckStatus',
        icon: mdiCheckOutline,
        visible: ['auth'],
    },
    {
        name: 'api.status_vip',
        title: 'StatusVip',
        icon: mdiStarFace,
        visible: ['auth'],
    },
    {
        name: 'api.trading_platform',
        title: 'TradingPlatform',
        icon: mdiCashMultiple,
        visible: ['auth'],
    },
    {
        name: 'api.transaction',
        title: 'Transaction',
        icon: mdiDatabaseExport,
        visible: ['auth'],
    },
    {
        name: 'api.type_trading_account',
        title: 'TypeTradingAccount',
        icon: mdiAccountChildOutline,
        visible: ['auth'],
    },
    {
        name: 'api.user',
        title: 'User',
        icon: mdiHomeAccount,
        visible: ['auth'],
    },
];

const menuStructure = [
    {
        name: 'home',
        title: 'Главная',
        icon: mdiHome,
        visible: ['guest', 'auth'],
    },
    {
        name: 'login',
        title: 'Войти',
        icon: mdiLogin,
        visible: ['guest'],
    },
    {
        name: 'logout',
        title: 'Выйти',
        icon: mdiLogout,
        visible: ['auth'],
    },
    {
        name: 'register',
        title: 'Зарегистрироваться',
        icon: mdiAccountPlus,
        visible: ['guest'],
    },
    {
        name: 'api.pages',
        title: 'Сущности',
        icon: mdiAlienOutline,
        child: entityMenu,
        visible: ['auth'],
    },
];

export default menuStructure;
