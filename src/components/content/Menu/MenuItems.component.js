'use strict';

import menuStructure from '@/components/content/Menu/menuStructure';
import {routes as backendRoutes} from '@/router/backendRoutes';
import Identity from '@/components/Security/Identity/Identity.type';
import { mapState } from 'vuex';

export default {
    name: 'MenuItems',

    computed: {
        ...mapState({
            /**
             * Флаг указывающий, авторизован ли пользователь в данный момент
             * @param isAuth
             * @return {boolean}
             */
            userIsAuth: ({authentication: {isAuth}}) => isAuth,
        }),
    },

    methods: {
        /**
         * Отображать ли текущий пункт меню
         * @param {{name: string, title: string, icon: string, visible: Array<string>}} route
         * @return {boolean}
         */
        isVisibleMenuItem (route) {
            let isVisible = false;
            if (this.userIsAuth) {
                if (route.visible.includes('auth')) {
                    if (!backendRoutes[route.name]) {
                        return true;
                    }

                    let seekRoleList = backendRoutes[route.name].roles;
                    isVisible = this.identity.can(seekRoleList);
                }
            } else {
                isVisible = route.visible.includes('guest');
            }

            return isVisible;
        },
    },

    data: () => ({
        backendRoutes,
        identity: new Identity(JSON.parse(localStorage.getItem('identity'))),
        menu: menuStructure,
    }),
};
