'use strict';

import { mapState } from 'vuex';
import frontendRoutes from '@/router/frontendRoutes';
import MenuItems from '@/components/content/Menu/MenuItems';

export default {
    name: 'Menu',

    components: {
        MenuItems,
    },

    computed: {
        ...mapState({
            /**
             * Флаг указывающий, авторизован ли пользователь в данный момент
             * @param isAuth
             * @return {boolean}
             */
            userIsAuth: ({authentication: {isAuth}}) => isAuth,

            /**
             * Объект с данными авторизованного пользователя
             * @param identity
             * @return {Identity}
             */
            identity: ({identity: {identity}}) => identity,
        }),
    },

    methods: {
        login() {
            this.$router.push(frontendRoutes('login'));
        },
        logout: function () {
            this.$store.dispatch('authentication/logout');
            this.$router.push(frontendRoutes('login'));
        },
    },

    data: () => ({
        drawer: null,
    }),
};
