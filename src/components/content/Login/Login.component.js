'use strict';

import frontendRoutes from '@/router/frontendRoutes';

export default {
    name: 'Login',

    computed: {
        isLoading: ({$store: {state: {authentication: {isLoading}}}}) => isLoading,
        formIsValid () {
            let formIsValid = true;

            if (!this.form.username) {
                formIsValid = false;
            }

            if (!this.form.password) {
                formIsValid = false;
            }

            return formIsValid;
        },
    },

    created() {
        // Отправляется действие vuex, которое выводит пользователя из системы,
        // если он авторизован, что позволяет также использовать страницу входа
        // в качестве страницы выхода из системы.
        if (localStorage.getItem('authentication-token')) {
            this.$router.push(frontendRoutes('home'));
        }
    },

    data: () => ({
        form: {
            username: localStorage.getItem('identity') && localStorage.getItem('identity').username,
            password: '',
            submitted: false,
        },
        rules: {
            username: [val => (val || '').length > 0 || 'This field is required'],
            password: [val => (val || '').length > 0 || 'This field is required'],
        },
    }),

    methods: {
        login() {
            if (this.formIsValid) {
                this.$store.dispatch('authentication/login', {
                    username: this.form.username,
                    password: this.form.password,
                });
            }
        },
    },
};
