'use strict';

import BrokerService from '@/components/content/pages/Broker/Broker.service';
import SnackbarType, { COLOR_ERROR, COLOR_SUCCESS, COLOR_WARNING } from '@/components/Snackbar/Snackbar.type';

class Actions {
    /**
     * Инициализация экшенов
     * @returns {{getBrokerList: *, getBrokerTotal: *}}
     */
    init() {
        const self = this;
        return {
            addBroker: self.addBroker,
            saveBroker: self.saveBroker,
            deleteBroker: self.deleteBroker,
            getBrokerList: self.getBrokerList,
            getBrokerTotal: self.getBrokerTotal,
        };
    }

    /**
     * FIXME: Доделать
     * Добавить нового брокера
     * @param {function} commit
     * @param {BrokerType} broker
     */
    addBroker({commit}, broker) {
        console.log(commit, broker);
        debugger; // FIXME: delete before deploy!
        // const brokerService = new BrokerService();
        // brokerService.addBroker()
        //     .then(() => {commit('ADD_BROKER_SUCCESS', broker);})
        //     .catch(error => {commit('ADD_BROKER_FAILURE', error);});
    }

    /**
     * Сохранить изменения в брокере
     * @param {function} commit
     * @param {function} dispatch
     * @param {BrokerType} broker
     */
    saveBroker({commit, dispatch}, broker) {
        commit('IS_LOADING_SAVE_BROKER', true);
        const brokerService = new BrokerService();
        brokerService.saveBroker(broker)
            .then(() => {
                commit('SAVE_BROKER_SUCCESS', broker);
                commit('IS_LOADING_SAVE_BROKER', false);
                dispatch('snackbar/show', new SnackbarType({
                    text: 'Брокер успешно сохранен',
                    color: COLOR_SUCCESS,
                }), {root: true});
            })
            .catch(errorMessages => {
                commit('SAVE_BROKER_FAILURE', errorMessages);
                commit('IS_LOADING_SAVE_BROKER', false);

                // TODO: переделать что бы каждая ошибка вылазила в отдельном snackbar после того
                // TODO: как разрабы Vuetify допилять snackbar (сейчас за раз можно показывать только один snackbar)
                dispatch('snackbar/show', new SnackbarType({
                    text: errorMessages.join(' '),
                    color: COLOR_ERROR,
                }), {root: true});
            });
    }

    /**
     * FIXME: Доделать
     * Удалить брокера
     * @param {function} commit
     * @param {BrokerType} broker
     */
    deleteBroker({commit}, broker) {
        console.log(commit, broker);
        debugger; // FIXME: delete before deploy!
        // const brokerService = new BrokerService();
        // brokerService.deleteBroker()
        //     .then(brokerTotal => {commit('GET_BROKER_TOTAL_SUCCESS', brokerTotal);})
        //     .catch(error => {commit('GET_BROKER_TOTAL_FAILURE', error);});
    }

    /**
     * Получить список брокеров
     * @param {function} commit
     * @param {function} dispatch
     */
    getBrokerList({commit, dispatch}) {
        commit('IS_LOADING_TABLE', true);

        const brokerService = new BrokerService();
        brokerService.getBrokerList().then(brokerList => {
            if (brokerList.length) {
                commit('GET_BROKER_LIST_SUCCESS', brokerList);
                commit('IS_LOADING_TABLE', false);
                return brokerList;
            }

            commit('IS_LOADING_TABLE', false);
            dispatch('snackbar/show', new SnackbarType({
                text: 'С Сервера пришел пустой список брокеров!',
                color: COLOR_WARNING,
            }), {root: true});
        }).catch(errorMessage => {
            commit('GET_BROKER_LIST_FAILURE', errorMessage);
            commit('IS_LOADING_TABLE', false);
            dispatch('snackbar/show', new SnackbarType({
                text: errorMessage || 'Произошла ошибка при получении брокеров!',
                color: COLOR_ERROR,
            }), {root: true});
        });
    }

    /**
     * Получить общее количество брокеров
     * @param {function} commit
     * @param {function} dispatch
     */
    getBrokerTotal({commit, dispatch}) {
        const brokerService = new BrokerService();
        brokerService.getBrokerTotal().then(brokerTotal => {
            commit('GET_BROKER_TOTAL_SUCCESS', brokerTotal);
        }).catch(error => {
            commit('GET_BROKER_TOTAL_FAILURE', error);
            dispatch('snackbar/show', new SnackbarType({
                text: 'С сервера не пришло общее количество брокеров.',
                color: COLOR_WARNING,
            }), {root: true});
        });
    }
}

export default Actions;
