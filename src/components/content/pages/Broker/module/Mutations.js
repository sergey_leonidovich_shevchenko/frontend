'use strict';

import { objectColumn } from '@/helpers';
import BrokerType from '@/models/entity/Broker.type';

class Mutations {
    /**
     * Инициализация мутаций
     * @returns {{}}
     */
    init() {
        const self = this;
        return {
            ADD_BROKER_SUCCESS: self.ADD_BROKER_SUCCESS,
            ADD_BROKER_FAILURE: self.ADD_BROKER_FAILURE,

            CLEAR_BROKER_LIST: self.CLEAR_BROKER_LIST,

            DELETE_BROKER_SUCCESS: self.DELETE_BROKER_SUCCESS,
            DELETE_BROKER_FAILURE: self.DELETE_BROKER_FAILURE,

            GET_BROKER_LIST_SUCCESS: self.GET_BROKER_LIST_SUCCESS,
            GET_BROKER_LIST_FAILURE: self.GET_BROKER_LIST_FAILURE,

            GET_BROKER_TOTAL_SUCCESS: self.GET_BROKER_TOTAL_SUCCESS,
            GET_BROKER_TOTAL_FAILURE: self.GET_BROKER_TOTAL_FAILURE,

            IS_LOADING_SAVE_BROKER: self.IS_LOADING_SAVE_BROKER,
            IS_LOADING_TABLE: self.IS_LOADING_TABLE,

            SAVE_BROKER_SUCCESS: self.SAVE_BROKER_SUCCESS,
            SAVE_BROKER_FAILURE: self.SAVE_BROKER_FAILURE,
        };
    }

    /**
     * Мутация вызывается при успешном добавлении брокера
     * @param state
     */
    ADD_BROKER_SUCCESS = function (state) {
        console.log(state);
        debugger; // FIXME: delete before deploy!
    };

    /**
     * Мутация вызывается при ошибки добавления брокера
     * @param state
     */
    ADD_BROKER_FAILURE = function (state) {
        console.log(state);
        debugger; // FIXME: delete before deploy!
    };

    /**
     * Мутация очищающая список брокеров
     * @param state
     */
    CLEAR_BROKER_LIST = function (state) {
        state.brokerList = [];
    };

    /**
     * Мутация вызывается при успешном удалении брокера
     * @param state
     */
    DELETE_BROKER_SUCCESS = function (state) {
        console.log(state);
        debugger; // FIXME: delete before deploy!
    };

    /**
     * Мутация вызывается при ошибки удаления брокера
     * @param state
     */
    DELETE_BROKER_FAILURE = function (state) {
        console.log(state);
        debugger; // FIXME: delete before deploy!
    };

    /**
     * Мутация вызывается при успешной загрузки списка брокеров
     * @param state
     * @param {{}[]|[]} brokerList
     */
    GET_BROKER_LIST_SUCCESS = function (state, brokerList) {
        /** @type {BrokerType[]|[]} Новый список брокеров для записи в state */
        let newBrokerList = [],
            /** @type {number[]|[]} Список Id брокеров находящихся в state */
            brokerInStateIds = [],
            /** @type {BrokerType|null} Брокер пришедший с сервера */
            brokerFromServer = null;

        newBrokerList = state.brokerList;
        brokerInStateIds = state.brokerList.length
            ? objectColumn(state.brokerList, 'id')
            : [];

        brokerList.forEach(brokerFromServerDirty => {
            if (!state.brokerList.length) {
                // Если в state нет брокеров, то устанавливаем новый список брокеров
                // (впервые загружаем брокеров)
                newBrokerList.push(new BrokerType(brokerFromServerDirty));
            } else {
                // Если в state уже есть брокеры,
                // то подгружаем к ним новый список брокеров,
                brokerFromServer = new BrokerType(brokerFromServerDirty);
                // Защищаемся от дублирования брокеров
                if (!brokerInStateIds.includes(brokerFromServer.id)) {
                    brokerInStateIds.push(brokerFromServer.id);
                    newBrokerList.push(brokerFromServer);
                    // Сортируем брокеров по id
                    newBrokerList.sort(({id: left}, {id: right}) => {
                        if (left > right) {
                            return 1;
                        } else if (right > left) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            }
        });

        // Ложим новый список брокеров в state
        state.brokerList = newBrokerList;
        
        if (process.env['NODE_ENV'] === 'development') {
            console.log(`Подгружен список брокеров - ${newBrokerList}!`);
        }
    };

    /**
     * Мутация вызывающаяся при ошибки получения списка брокеров
     * @param state
     * @param {string} error
     */
    GET_BROKER_LIST_FAILURE = function (state, error) {
        if (process.env['NODE_ENV'] === 'development') {
            console.log('Произошла ошибка при загрузке брокеров!', error);
        }
    };

    /**
     * Мутация устанавливающая общее количество брокеров
     * (Нужен для пагинации)
     * @param state
     * @param {number} brokerTotal
     */
    GET_BROKER_TOTAL_SUCCESS = function (state, brokerTotal) {
        state.brokerTotal = brokerTotal;
        if (process.env['NODE_ENV'] === 'development') {
            console.log(`Подгружено количество брокеров - ${brokerTotal}!`);
        }
    };

    /**
     * Мутация вызывающаяся при ошибки получения общего количества брокеров
     * @param state
     * @param {string} error
     */
    GET_BROKER_TOTAL_FAILURE = function (state, error) {
        state.brokerTotal = 0;
        if (process.env['NODE_ENV'] === 'development') {
            console.log('Произошла ошибка при загрузке количества брокеров!', error);
        }
    };

    /**
     * Мутация для изменения ключа говорящего,
     * что в данный момент идет сохранения брокера
     * @param state
     * @param {boolean} isLoadingSaveBroker
     */
    IS_LOADING_SAVE_BROKER = function (state, isLoadingSaveBroker) {
        state.isLoadingSaveBroker = isLoadingSaveBroker;
    };

    /**
     * Мутация для изменения ключа говорящего,
     * что в данный момент идет загрузка с сервера
     * @param state
     * @param {boolean} isLoadingTable
     */
    IS_LOADING_TABLE = function (state, isLoadingTable) {
        state.isLoadingTable = isLoadingTable;
    };

    /**
     * Мутация вызывается при успешном сохранении брокера
     * @param state
     */
    SAVE_BROKER_SUCCESS = function (state) {
        // Закрываем окно редактирования брокера
        state.dialogEditBroker = false;
    };

    /**
     * Мутация вызывается при ошибки сохранения брокера
     * @param state
     */
    SAVE_BROKER_FAILURE = function (state) {
        console.log(state);
    };
}

export default Mutations;
