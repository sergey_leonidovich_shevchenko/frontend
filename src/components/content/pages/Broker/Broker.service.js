'use strict';

import backendRoutes from '@/router/backendRoutes';
import BrokerType from '@/models/entity/Broker.type';
import RequestService from '@/services/Request.service';

class BrokerService {
    /**
     * Сервис для отправки запросов на сервер
     * @type {RequestService}
     * @private
     */
    _requestService = null;

    /** @returns {RequestService} */
    get requestService() {
        return this._requestService;
    }

    /** @constructor */
    constructor() {
        this._requestService = new RequestService();
    }

    /**
     * Получить список брокеров с сервера
     * @returns {{}[]}
     */
    async getBrokerList() {
        const {url} = backendRoutes('api.broker');
        return await this.requestService.sendAsync(url)
            .catch(({message}) => {
                    throw new Error(message);
                },
            );
    }

    /**
     * Получить общее количество брокеров с сервера
     * @returns {number}
     */
    async getBrokerTotal() {
        const {url} = backendRoutes('api.broker.total');
        return await this.requestService.sendAsync(url).then(({data: {total}}) => total, () => 0);
    }

    /**
     * Добавить нового брокера на сервере
     * @param {BrokerType} broker
     * @returns {boolean}
     */
    async addBroker(broker) {
        this._assertBroker(broker);

        const {url} = backendRoutes('api.broker');
        return await this.requestService.sendAsync(url, this.requestService.METHOD_POST)
            .then(({status}) => status === 200)
            .catch(() => false);
    }

    /**
     * Сохранить брокера на сервере
     * @param {BrokerType} broker
     * @returns {boolean} Результат сохранения брокера на сервере
     */
    async saveBroker(broker) {
        this._assertBroker(broker);

        const {url} = backendRoutes('api.broker') + '/' + broker.id;
        return await this.requestService.sendAsync(url, this.requestService.METHOD_PUT, broker)
            .then(({status}) => {
                if (status === 200) {
                    return true;
                }
                throw new Error('При сохранении брокера произошла неизвестная ошибка');
            })
            .catch(({data: {messages: errorMessages}}) => {
                throw errorMessages;
            });
    }

    /**
     * Удалить брокера на сервере
     * @param {BrokerType} broker
     * @returns {boolean}
     */
    async deleteBroker(broker) {
        this._assertBroker(broker);

        const {url} = backendRoutes('api.broker') + '/' + broker.id;
        return await this.requestService.sendAsync(url, this.requestService.METHOD_DELETE, broker)
            .then(({status}) => status === 204)
            .catch(() => false);
    }

    /**
     * Проверка является ли объект экземпляром класса BrokerType
     * @param {BrokerType} broker
     * @private
     */
    _assertBroker(broker) {
        if (!(broker instanceof BrokerType)) {
            throw new Error('Error: Broker is not an instance of a BrokerType class');
        }
    }
}

export default BrokerService;
