'use strict';

import Actions from './module/Actions';
import Mutations from './module/Mutations';

class Broker {
    actions = new Actions().init();
    getters = this.initGetters();
    mutations = new Mutations().init();
    namespaced = true;
    state = this.initState();

    /**
     * Инициализация геттеров
     * @returns {{brokerTotal: (function(*): number), brokerList: (function(*): Broker[])}}
     */
    initGetters() {
        return {
            brokerList: ({brokerList}) => brokerList,
            brokerTotal: ({brokerTotal}) => brokerTotal,

            dialogEditBroker: ({dialogEditBroker}) => dialogEditBroker,

            isLoadingSaveBroker: ({isLoadingSaveBroker}) => isLoadingSaveBroker,
            isLoadingTable: ({isLoadingTable}) => isLoadingTable,
        };
    }

    /**
     * Инициализация состояния
     * @returns {{brokerTotal: number, brokerList: []}}
     */
    initState() {
        return {
            brokerList: [], // Список брокеров
            brokerTotal: 0, // Количество брокеров в БД

            dialogEditBroker: false, // Открыт ли диалог изменения/добавления брокера

            isLoadingSaveBroker: false, // Происходит ли сохранение брокера в текущий момент
            isLoadingTable: false, // Происходит ли загрузка/подгрузка данных в таблицу
        };
    }
}

export default Broker;
