'use strict';

import BrokerType from '@/models/entity/Broker.type';
import configEnv from '@/configEnv';
import moment from 'moment';

export default {
    /** Динамически вычисляемые свойства компонента */
    computed: {
        /**
         * Список с брокерами
         * (наполнение таблицы)
         * @param $store
         * @returns {BrokerType[]}
         */
        brokerList: ({$store: {state: {broker: {brokerList}}}}) => brokerList,

        /**
         * Общее количество брокеров в БД
         * (для пагинации)
         * @param $store
         * @returns {number}
         */
        brokerTotal: ({$store: {state: {broker: {brokerTotal}}}}) => brokerTotal,

        /**
         * Название модального окна в зависимости от действия
         * @returns {string}
         */
        modalFormTitle: function () {
            return this.broker.edited.index >= 0
                ? 'Редактирование брокера'
                : 'Добавление нового брокера';
        },

        /**
         * Флаг указывающий на то, что происходит сохранение брокера
         */
        isLoadingSaveBroker: ({$store: {state: {broker: {isLoadingSaveBroker}}}}) => isLoadingSaveBroker,

        /**
         * Флаг указывающий на загрузку данных с сервера
         * (для начальной загрузки таблицы и подгрузки в нее данных при пагинации)
         * @param $store
         * @returns {boolean}
         */
        isLoadingTable: ({$store: {state: {broker: {isLoadingTable}}}}) => isLoadingTable,
    },

    /**
     * Функция исполняемая при создании компонента
     */
    created: function () {
        // Подгрузить список брокеров в таблицу
        this.$store.dispatch('broker/getBrokerList')
            .catch(reason => alert(reason));

        // Подгрузить общее число брокеров в пагинацию таблицы
        this.$store.dispatch('broker/getBrokerTotal')
            .catch(reason => alert(reason));
    },

    /**
     * Начальные данные перед инициализации компонента
     * @returns {*}
     */
    data: () => ({
        broker: {
            // Копия редактируемого объекта нужна, что бы когда при изменении брокера
            // и закрыти модального окна измененные данные не сохранились
            default: new BrokerType({}),
            edited: {
                // Редактируемая в настоящий момент копия брокера
                self: new BrokerType({}),

                // Индекс редактируемого брокера (-1 значит брокер создается)
                index: -1,
            },
            editorType: -1,
            fields: {
                createdAt: {
                    date: null,       // Модель даты (требуется для пикера)
                    dateHuman: '',    // Дата в отформатированном виде для показа человеку // TODO: можно форматировать в месте вывода и убрать здесь это свойство
                    dateMoment: null, // Объект даты moment
                    time: null,       // Модель времени (требуется для пикера)
                    timeHuman: '',    // Время в отформатированном виде для показа человеку // TODO: можно форматировать в месте вывода и убрать здесь это свойство
                    timeMoment: null, // Объект времени moment
                },
                updatedAt: {
                    date: null,       // Модель даты (требуется для пикера)
                    dateHuman: '',    // Дата в отформатированном виде для показа человеку // TODO: можно форматировать в месте вывода и убрать здесь это свойство
                    dateMoment: null, // Объект даты moment
                    time: null,       // Модель времени (требуется для пикера)
                    timeHuman: '',    // Время в отформатированном виде для показа человеку // TODO: можно форматировать в месте вывода и убрать здесь это свойство
                    timeMoment: null, // Объект времени moment
                },
            },
        },

        // Данные по диалоговым окнам
        dialog: {
            change: {
                broker: {
                    // Диалог изменения брокера
                    self: false,

                    // Диалоги изменения даты и времени создания брокера
                    createdAt: {
                        date: false,
                        time: false,
                    },

                    // Диалоги изменения даты и времени обновления брокера
                    updatedAt: {
                        date: false,
                        time: false,
                    },
                },
            },
        },

        // Данные по кнопкам
        events: {
            isChange: {
                broker: {
                    // Было ли изменение в брокере
                    self: false,

                    createdAt: {
                        // Была ли изменена дата создания брокера
                        date: false,
                        // Было ли изменено время создания брокера
                        time: false,
                    },

                    updatedAt: {
                        // Была ли изменена дата последнего обновления брокера
                        date: false,
                        // Было ли изменено время последнего обновления брокера
                        time: false,
                    },
                },
            },
        },

        // Настройки таблицы (см https://vuetifyjs.com/en/components/data-tables)
        table: {
            // Настройка футера таблицы
            footerProps: {
                showCurrentPage: true,
                showFirstLastPage: true,
                firstIcon: 'mdi-arrow-collapse-left',
                lastIcon: 'mdi-arrow-collapse-right',
                prevIcon: 'mdi-arrow-left',
                nextIcon: 'mdi-arrow-right',
                itemsPerPageOptions: [5, 25, 50, 100, 200], // TODO: Вынести в общие настройки
            },

            // Заголовки таблицы
            headers: [
                {text: 'ID', value: 'id', align: 'center'},
                {text: 'Название', value: 'name', align: 'center'},
                {text: 'Создан', value: 'createdAt', align: 'center'},
                {text: 'Обновлен', value: 'updatedAt', align: 'center'},
                {text: 'Настройки', value: 'data-table-expand', align: 'center'},
                {text: '', value: 'action', align: 'center', sortable: false},
            ],

            // Пагинация и сортировка
            options: {
                // Порядок сортировки
                ascending: true,

                // По какой колонке производить сортировку брокеров
                // sortBy: 'id',

                // Количество брокеров на страницу
                itemsPerPage: configEnv('TABLE_ENTITY_BROKER_PER_PAGE')
                    || configEnv('TABLE_ENTITY_PER_PAGE')
                    || 25,

                // Номер страницы
                page: 1,
            },

            // Данные по поиску
            search: {
                // Текст для поиска
                text: '',
            },

            // Список с выбранными брокерами
            selected: [],
        },
    }),

    /**
     * Методы компонента
     */
    methods: {
        /** Проверить произошли ли изменения в брокере (при редактировании) */
        checkBrokerOnChange() {
            const self = this;
            let edited = false;

            // TODO: перенести в сервисы (как diffBroker | diffEntity) или хелперы (как diffObject)

            const brokerEdited = self.broker.edited.self;
            if (!(brokerEdited instanceof BrokerType)) {
                return null;
            }

            const brokerDefault = self.broker.default;
            if (!(brokerDefault instanceof BrokerType)) {
                return null;
            }

            Object.keys(brokerEdited).forEach(key => {
                const typeCurrentField = typeof brokerEdited[key];

                // Если текущее поле относится к скалярному типу данных
                let isScalarType = ['number', 'string', 'boolean'].includes(typeCurrentField);
                if (isScalarType) {
                    if (brokerEdited[key] !== brokerDefault[key]) {
                        edited = true;
                        return; // continue
                    }
                    return; // continue
                }

                let isMomentType = brokerEdited[key] instanceof moment;
                if (isMomentType) {
                    if (brokerEdited[key].diff(brokerDefault[key])) {
                        return;
                    }
                    return null;
                }
            });
            self.events.isChange.broker.self = edited;
        },

        //////////////////////////////////////
        // Методы открытия диалоговых окон //
        ////////////////////////////////////
        /**
         * Редактирование брокера
         * @param {BrokerType|{}} broker
         */
        dialogOpenChangeBroker(broker = {}) {
            const self = this;

            // Поиск выбранного брокера в state
            self.broker.edited.index = self.brokerList.indexOf(broker);
            self.broker.default = Object.assign(new BrokerType({}), broker);
            self.broker.edited.self = broker;

            if (broker) {
                // Приводим дату и время к нужному виду для показа в пикерах
                ['createdAt', 'updatedAt'].forEach(field => {
                    self.broker.fields[field].date = broker[field] && broker[field].format('YYYY-MM-DD');
                    self.broker.fields[field].dateHuman = broker[field] && broker[field].format('DD.MM.YYYY');
                    self.broker.fields[field].dateMoment = broker[field] && broker[field];
                    self.broker.fields[field].time = broker[field] && broker[field].format('hh:mm:ss');
                    self.broker.fields[field].timeHuman = broker[field] && broker[field].format('hh:mm:ss');
                    self.broker.fields[field].timeMoment = broker[field] && broker[field];
                });
            }

            // Делаем что бы кнопка "сохранить" была везде неактивной
            // (пока не внесут изменения в брокера)
            ['updatedAt', 'createdAt'].forEach(field => {
                ['date', 'time'].forEach(subField => {
                    self.events.isChange.broker[field][subField] = false;
                });
            });
            self.events.isChange.broker.self = false;

            // Показываем деалоговое окно для редактирования брокера
            self.dialog.change.broker.self = true;
        },

        //////////////////////////////////////
        // Методы закрытия диалоговых окон //
        ////////////////////////////////////
        /**
         * Закрытие диалогового окна изменения брокера (без применении изменений)
         */
        dialogCloseChangeBroker() {
            this.dialog.change.broker.self = false;
            setTimeout(() => {
                // this.broker.edited = this.broker.default;
                this.broker.editorType = -1;
            }, 300);
        },

        /**
         * Закрытие диалогового окна изменения даты создания брокера
         */
        dialogCloseChangeBrokerCreatedAtDate() {
            this.dialog.change.broker.createdAt.date = false;
        },

        /**
         * Закрытие диалогового окна изменения времени создания брокера
         */
        dialogCloseChangeBrokerCreatedAtTime() {
            this.dialog.change.broker.createdAt.time = false;
        },

        /**
         * Закрытие диалогового окна изменения даты последнего обновления брокера
         */
        dialogCloseChangeBrokerUpdatedAtDate() {
            this.dialog.change.broker.updatedAt.date = false;
        },

        /**
         * Закрытие диалогового окна изменения времени последнего обновления брокера
         */
        dialogCloseChangeBrokerUpdatedAtTime() {
            this.dialog.change.broker.updatedAt.time = false;
        },

        ////////////////////////////////////////////////////
        // Методы сохранения изменений в диалоговых окон //
        //////////////////////////////////////////////////
        /**
         * Применений изменений брокера
         * @param {BrokerType} broker
         */
        dialogSaveChangeBroker(broker) {
            const self = this;
            // self.broker.edited.index = self.brokerList.indexOf(broker);
            self.$store.dispatch('broker/saveBroker', broker)
                .then(() => {
                    // FIXME: Сюда заходит не дожидаясь результата который в dispatch
                    self.brokerList.splice(self.broker.edited.index, 1); // FIXME: переделать через мутации!
                });
        },

        /**
         * Применение изменений и закрытия диалогового окна
         * по редактированию даты создания брокера
         */
        dialogSaveChangeBrokerCreatedAtDate() {
            let date = this.broker.fields.createdAt.dateHuman;
            this.$refs.createdAtDate.save(date);
            this.checkBrokerOnChange();
        },

        /**
         * Применение изменений и закрытия диалогового окна
         * по редактированию времени создания брокера
         */
        dialogSaveChangeBrokerCreatedAtTime() {
            let time = this.broker.fields.createdAt.dateHuman;
            this.$refs.createdAtDate.save(time);
            this.checkBrokerOnChange();
        },

        /**
         * Применение изменений и закрытия диалогового окна
         * по редактированию даты последнего изменения брокера
         */
        dialogSaveChangeBrokerUpdatedAtDate() {
            let date = this.broker.fields.updatedAt.dateHuman;
            this.$refs.updatedAtDate.save(date);
            this.checkBrokerOnChange();
        },

        /**
         * Применение изменений и закрытия диалогового окна
         * по редактированию времени последнего изменения брокера
         */
        dialogSaveChangeBrokerUpdatedAtTime() {
            let time = this.broker.fields.updatedAt.dateHuman;
            this.$refs.updatedAtDate.save(time);
            this.checkBrokerOnChange();
        },

        /**
         * Удаление брокера
         * @param broker
         */
        deleteBroker(broker) {
            debugger; // FIXME: CHECK IF THIS METHOD IS USED
            const confirmMessage = 'Вы действительно хотите удалить этого брокера? \n'
                + '[' + broker.id + '] ' + broker.name;
            confirm(confirmMessage)
            && this.$store.dispatch('broker/deleteBroker', broker).catch(reason => alert(reason));
            this.brokerList.splice(this.broker.edited.index, 1);
        },

        /** Обновить таблицу */
        tableRefresh() {
            this.$store.dispatch('broker/getBrokerList').catch(reason => alert(reason));
        },
    },

    /** Название компонента */
    name: 'Broker',

    watch: {
        /** Событие изменения даты создания брокера */
        'events.isChange.broker.createdAt.date'(oldVal, newVal) {
            if (oldVal === newVal) {
                // UNSET this block if this dead code
                debugger;
                return;
            }
            this.checkBrokerOnChange();
            // this.events.isChange.broker.self = true;
        },

        /** Событие изменения времени создания брокера */
        'events.isChange.broker.createdAt.time'(oldVal, newVal) {
            if (oldVal === newVal) {
                // UNSET this block if this dead code
                debugger;
                return;
            }
            this.checkBrokerOnChange();
            // this.events.isChange.broker.self = true;
        },

        /** Событие изменения даты обновления брокера */
        'events.isChange.broker.updatedAt.date'(oldVal, newVal) {
            if (oldVal === newVal) {
                // UNSET this block if this dead code
                debugger;
                return;
            }
            this.checkBrokerOnChange();
            // this.events.isChange.broker.self = true;
        },

        /** Событие изменения времени обновления брокера */
        'events.isChange.broker.updatedAt.time'(oldVal, newVal) {
            if (oldVal === newVal) {
                // UNSET this block if this dead code
                debugger;
                return;
            }
            this.checkBrokerOnChange();
            // this.events.isChange.broker.self = true;
        },

        // TODO: down records to DRY?
        /**
         * При изменении даты последнего обновления брокера
         * устанавливать в текстовое поле установленную дату в нужном формате
         * @param {string} newDate 'YYYY-MM-DD'
         * @param {string|date} oldDate
         */
        'broker.fields.updatedAt.date'(newDate, oldDate) {
            if (!oldDate || newDate === oldDate) {
                return;
            }

            oldDate = this.broker.fields.updatedAt.dateHuman;
            if (oldDate !== newDate) {
                let [year, month, day] = newDate.split('-');
                this.broker.fields.updatedAt.dateHuman = `${day}.${month}.${year}`; // DD.MM.YYYY
                this.broker.fields.updatedAt.dateMoment.date(day).month(month).year(year);
                this.events.isChange.broker.updatedAt.date = true;
            }
        },

        /**
         * При изменении времени последнего обновления брокера
         * устанавливать в текстовое поле установленное время в нужном формате
         * @param {string} newTime
         * @param {string|null} oldTime
         */
        'broker.fields.updatedAt.time'(newTime, oldTime) {
            if (!oldTime || newTime === oldTime) {
                return;
            }

            oldTime = this.broker.fields.updatedAt.timeHuman;
            if (oldTime !== newTime) {
                let [hours, minutes, seconds] = newTime.split(':');
                this.broker.fields.updatedAt.timeHuman = `${hours}:${minutes}:${seconds}`; // hh:mm:ss
                this.broker.fields.updatedAt.timeMoment.hours(hours).minutes(minutes).seconds(seconds);
                this.events.isChange.broker.updatedAt.time = true;
            }
        },

        /**
         * При изменении даты создания брокера
         * устанавливать в текстовое поле установленную дату в нужном формате
         * @param {string} newDate 'YYYY-MM-DD'
         * @param {string|date} oldDate
         */
        'broker.fields.createdAt.date'(newDate, oldDate) {
            if (!oldDate || newDate === oldDate) {
                return;
            }

            oldDate = this.broker.fields.createdAt.dateHuman;
            if (oldDate !== newDate) {
                let [year, month, day] = newDate.split('-');
                this.broker.fields.createdAt.dateHuman = `${day}.${month}.${year}`; // DD.MM.YYYY
                this.broker.fields.createdAt.dateMoment.date(day).month(month).year(year);
                this.events.isChange.broker.createdAt.date = true;
            }
        },

        /**
         * При изменении времени создания брокера
         * устанавливать в текстовое поле установленное время в нужном формате
         * @param {string} newTime
         * @param {string|null} oldTime
         */
        'broker.fields.createdAt.time'(newTime, oldTime) {
            if (!oldTime || newTime === oldTime) {
                return;
            }

            oldTime = this.broker.fields.createdAt.timeHuman;
            if (oldTime !== newTime) {
                let [hours, minutes, seconds] = newTime.split(':');
                this.broker.fields.createdAt.timeHuman = `${hours}:${minutes}:${seconds}`; // hh:mm:ss
                this.broker.fields.createdAt.timeMoment.hours(hours).minutes(minutes).seconds(seconds);
                this.events.isChange.broker.createdAt.time = true;
            }
        },
    },
};
