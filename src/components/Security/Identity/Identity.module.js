'use strict';

import Actions from './module/Actions';
import Mutations from './module/Mutations';
import Identity from '@/components/Security/Identity/Identity.type';

class IdentityModule {
    actions = new Actions().init();
    getters = this.initGetters();
    mutations = new Mutations().init();
    namespaced = true;
    state = this.initState();

    /**
     * Инициализация геттеров
     * @returns {{identity: (function({identity: Identity|null}): Identity|null)}}
     */
    initGetters() {
        return {
            /**
             * Объект авторизованного пользователя
             * @param identity|null
             * @return {Identity|null}
             */
            identity: ({identity}) => identity,
        };
    }

    /**
     * Инициализация состояния
     * @returns {{identity: null}}
     */
    initState() {
        return {
            identity: new Identity({}),
        };
    }
}

export default IdentityModule;
