'use strict';

import CheckGranted from '@/components/Security/CheckGranted';
import RoleHierarchy from '@/components/Security/RoleHierarhy';

/**
 * Сущность "Идентификация пользователя в системе"
 */
class Identity {
    /**
     * @type {number|null}
     * @private
     */
    _id = null;

    /**
     * @type {string|null}
     * @private
     */
    _username = null;

    /**
     * @type {string|null}
     * @private
     */
    _email = null;

    /**
     * @type {Array<string>}
     * @private
     */
    _roles = [];

    /**
     * @constructor
     * @param {number} id
     * @param {string} username
     * @param {string} email
     * @param {Array<string>} roles
     */
    constructor({id = null, username = null, email = null, roles = []}) {
        this._id = id;
        this._username = username;
        this._email = email;
        this._roles = roles;

        if (roles.length) {
            this.rearrangeRoles();
        }
    }

    /**
     * Добавить новую роль пользователю
     * @param {string} role
     */
    addRole(role) {
        if (!this.roles.includes(role)) {
            this.roles.push(role);
            return true;
        }

        return false;
    }

    /**
     * Проверяет есть ли текущая роль у пользователя
     * @param {Array<string>} roleList
     * @return {boolean}
     */
    can (roleList) {
        if (!roleList) {
            return false;
        }

        const checkGranted = new CheckGranted();
        return checkGranted.can(this, roleList);
    }

    /**
     * Реорганизация ролей таким образом,
     * что если у роли есть вложенные роли,
     * то достаем их и кладем в корневой уровень
     * Вход: [ROLE_USER_CRUD, ROLE_BROKER_GET]
     * Выход:[ROLE_USER_CRUD, ROLE_USER_CREATE, ROLE_USER_UPDATE, ROLE_USER_GET, ROLE_USER_DELETE, ROLE_BROKER_GET]
     */
    rearrangeRoles() {
        const oldIdentityRoles = this.roles;
        let newRoles = [];

        for (let role of oldIdentityRoles) {
            let containsRoles = RoleHierarchy.getContainsRoles(role);
            console.log('rearrangeRoles -> containsRoles:', containsRoles); // FIXME: delete before deploy!
        }

        this.roles = newRoles;
    }

    /**
     * @returns {number}
     */
    get id() {
        return this._id;
    }
    /**
     * @param {number} id
     */
    set id(id) {
        this._id = id;
    }

    /**
     * @returns {string}
     */
    get username() {
        return this._username;
    }
    /**
     * @param {string} username
     */
    set username(username) {
        this._username = username;
    }

    /**
     * @returns {string}
     */
    get email() {
        return this._email;
    }
    /**
     * @param {string} email
     */
    set email(email) {
        this._email = email;
    }

    /**
     * @returns {Array<string>}
     */
    get roles() {
        return this._roles;
    }
    /**
     * @param {Array<string>} roles
     */
    set roles(roles) {
        this._roles = roles.sort();
    }
}

export default Identity;
