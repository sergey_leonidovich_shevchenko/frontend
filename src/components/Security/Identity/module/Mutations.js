'use strict';

import Identity from '@/components/Security/Identity/Identity.type';

class Mutations {
    /**
     * Инициализация мутаций
     * @returns {{}}
     */
    init() {
        const self = this;
        return {
            SAVE_IDENTITY: self.SAVE_IDENTITY,
        };
    }

    /**
     * Мутация возникающая при сохранении информации об авторизованном пользователе
     * @param {{identity: Identity}} state
     * @param {Identity} identity
     * @constructor
     */
    SAVE_IDENTITY = (state, identity) => {
        identity = new Identity(identity);
        state.identity = identity;

        console.log(state, identity); // FIXME: delete before deploy!
    };
}

export default Mutations;
