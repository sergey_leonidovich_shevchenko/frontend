'use strict';

class Actions {
    /**
     * Инициализация экшенов
     * @returns {{save: *}}
     */
    init() {
        const self = this;
        return {
            save: self.save,
        };
    }

    /**
     * Сохранение сущности авторизованного пользователя
     * @param {function({string}, {...data})} commit Функция вызова мутации
     * @param {Identity} identity Объект с данными авторизованного пользователя
     */
    save({commit}, identity) {
        commit('SAVE_IDENTITY', identity);
    }
}

export default Actions;
