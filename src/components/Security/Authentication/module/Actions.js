'use strict';

import AuthenticationService from '../Authentication.service.js';
import frontendRoutes from '@/router/frontendRoutes';
import $router from '@/router';

class Actions {
    /**
     * Инициализация экшенов
     * @returns {{login: *, logout: *}}
     */
    init() {
        const self = this;
        return {
            login: self.login,
            logout: self.logout,
        };
    }

    /**
     * Аутентификация пользователя
     * @param {function({string}, {...data})} commit Функция вызова мутации
     * @param {string} username Логин пользователя
     * @param {string} password Пароль пользователя
     */
    login({commit}, {username, password}) {
        commit('IS_LOADING', true);
        const authService = new AuthenticationService();
        authService.login(username, password)
            .then(data => {
                commit('AUTH_SUCCESS', data);
                $router.push(frontendRoutes('home'));
            })
            .catch((message) => {
                commit('AUTH_FAILURE', message);
                return message;
            })
            .finally(() => commit('IS_LOADING', false));
    }

    /**
     * РазАутентификация пользователя
     * @param {function({string}, {...data})} commit Функция вызова мутации
     */
    logout({commit}) {
        commit('LOGOUT');
    }
}

export default Actions;
