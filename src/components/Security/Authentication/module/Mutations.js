'use strict';

import $store from '@/store';
import SnackbarType, { COLOR_INFO, COLOR_SUCCESS } from '@/components/Snackbar/Snackbar.type';
import { COLOR_ERROR } from '@/components/Snackbar/Snackbar.type';

class Mutations {
    /**
     * Инициализация мутаций
     * @returns {{}}
     */
    init() {
        const self = this;
        return {
            AUTH_SUCCESS: self.AUTH_SUCCESS,
            AUTH_FAILURE: self.AUTH_FAILURE,
            IS_LOADING: self.IS_LOADING,
            LOGOUT: self.LOGOUT,
        };
    }

    /**
     * Мутация возникающая при успешной аутентификации пользователя
     * @param {{isAuth: boolean, token: string, identity: Identity}} state
     * @param {string} token
     * @param {Identity} identity
     * @constructor
     */
    AUTH_SUCCESS = (state, {token , user: identity }) => {
        state.isAuth = true;
        state.token = token;

        $store.dispatch('identity/save', identity);

        localStorage.setItem('authentication-token', token);
        localStorage.setItem('identity', JSON.stringify(identity));

        $store.dispatch('snackbar/show', new SnackbarType({
            text: 'Авторизация прошла успешна!',
            color: COLOR_SUCCESS,
        }));
    };

    /**
     * Мутация возникающая при ошибке попытки аутентификации
     * @param {{isAuth: boolean, token: string}} state
     * @param {string} message
     * @constructor
     */
    AUTH_FAILURE = (state, message) => {
        state.isAuth = false;
        state.token = '';

        $store.dispatch('snackbar/show', new SnackbarType({
            text: message,
            color: COLOR_ERROR,
        }));
    };

    /**
     * Флаг говорящий, идет ли в данный момент запрос на сервер на аутентификацию (для прелоадера)
     * @param {{isLoading: boolean}} state
     * @param {boolean} isLoading
     * @constructor
     */
    IS_LOADING = (state, isLoading) => {
        state.isLoading = isLoading;
    };

    /**
     * Мутация разлогинивания пользователя
     * @param {{isAuth: boolean, token: string}} state
     * @constructor
     */
    LOGOUT = state => {
        state.isAuth = false;
        state.token = '';

        localStorage.removeItem('authentication-token');
        $store.dispatch('snackbar/show', new SnackbarType({
            text: 'Вы вышли из системы.',
            color: COLOR_INFO,
        }));
    };
}

export default Mutations;
