'use strict';

import Actions from './module/Actions';
import Mutations from './module/Mutations';

/**
 * Модуль аутентификации vuex отвечает за authentication раздел централизованного хранилища состояний.
 * Он содержит действия для входа и выхода из приложения,
 * а также мутации для каждого из изменений состояния нижнего уровня, связанных с каждым действием.
 * Исходное состояние пользователя, вошедшего в систему, устанавливается путем проверки,
 * сохранен ли пользователь в локальном хранилище, что позволяет пользователю войти в систему,
 * если браузер обновляется и между сеансами браузера.
 */
class AuthenticationModule {
    actions = new Actions().init();
    getters = this.initGetters();
    mutations = new Mutations().init();
    namespaced = true;
    state = this.initState();

    /**
     * Инициализация геттеров
     * @returns {{isAuth: boolean, isLoading: boolean, token: string, identity: Identity}}
     */
    initGetters() {
        return {
            isAuth: ({isAuth}) => isAuth,
            isLoading: ({isLoading}) => isLoading,
            token: ({token}) => token,
            identity: ({identity}) => identity,
        };
    }

    /**
     * Инициализация состояния
     * @returns {{}}
     */
    initState() {
        const token = localStorage.getItem('authentication-token') || '';
        const identity = localStorage.getItem('identity') || null;

        return {
            isAuth: !!token,
            isLoading: false,
            token,
            identity,
        };
    }
}

export default AuthenticationModule;
