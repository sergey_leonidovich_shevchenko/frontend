'use strict';

import backendRoutes from '@/router/backendRoutes';
import RequestService from '@/services/Request.service.js';
import SecurityHandlerService from '@/services/Handler/SecurityHandler.service';

/**
 * Пользовательский сервис инкапсулирует все вызовы API backend для выполнения операций CRUD
 * над пользовательскими данными, а также для регистрации и выхода из примера приложения.
 * Сервисные методы экспортируются через userService объект в верхней части файла,
 * а реализация каждого метода находится в функциях ниже.
 * В методе handleResponse служба проверяет, является ли http-ответ от API 401 Unauthorized,
 * и автоматически выводит пользователя из системы.
 * Это обрабатывает, если токен JWT истекает или больше не действителен по любой причине.
 */
class AuthenticationService {
    /**
     * Сервис для отправки запросов на сервер
     * @type {RequestService}
     * @private
     */
    _requestService = null;

    /** @constructor */
    constructor () {
        this._requestService = new RequestService();
    }

    /** @returns {RequestService} */
    get requestService() {
        return this._requestService;
    }

    /**
     * Метод отсылающий запрос на авторизацию пользователя (на получение токена)
     * @param username
     * @param password
     * @returns {boolean}
     */
    async login(username, password) {
        if (!username) {
            throw new Error('Username cannot be empty!');
        }
        if (!password) {
            throw new Error('Password cannot be empty!');
        }

        const {url} = backendRoutes('security.login');
        return await this._requestService.sendAsync(url, this.requestService.METHOD_POST, {username, password})
            .then(data => SecurityHandlerService.handlerLoginSuccess(data))
            .catch(data => SecurityHandlerService.handlerLoginError(data));
    }

    /**
     * Remove user data from local storage
     */
    logout() {
        localStorage.removeItem('authentication-token');
    }
}

export default AuthenticationService;
