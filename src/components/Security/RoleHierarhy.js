'use strict';

class RoleHierarchy {
    static ROLE_ROOT = 'ROLE_ROOT';
    static ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    static ROLE_ADMIN = 'ROLE_ADMIN';
    static ROLE_OFFICE_SUPPORT = 'ROLE_OFFICE_SUPPORT';
    static ROLE_SUPPORT_HALF = 'ROLE_SUPPORT_HALF';

    static ROLE_ENTITY_API_BROKER_CRUD = 'ROLE_ENTITY_API_BROKER_CRUD';
    static ROLE_BROKER_CREATE = 'ROLE_BROKER_CREATE';
    static ROLE_ENTITY_API_BROKER_GET = 'ROLE_ENTITY_API_BROKER_GET';
    static ROLE_ENTITY_API_BROKER_UPDATE = 'ROLE_ENTITY_API_BROKER_UPDATE';
    static ROLE_ENTITY_API_BROKER_DELETE = 'ROLE_ENTITY_API_BROKER_DELETE';
    static ROLE_ENTITY_API_BROKER_TOTAL = 'ROLE_ENTITY_API_BROKER_TOTAL';

    static ROLE_ENTITY_API_ACCOUNT_CRUD = 'ROLE_ENTITY_API_ACCOUNT_CRUD';
    static ROLE_ACCOUNT_CREATE = 'ROLE_ACCOUNT_CREATE';
    static ROLE_ENTITY_API_ACCOUNT_GET = 'ROLE_ENTITY_API_ACCOUNT_GET';
    static ROLE_ENTITY_API_ACCOUNT_UPDATE = 'ROLE_ENTITY_API_ACCOUNT_UPDATE';
    static ROLE_ENTITY_API_ACCOUNT_DELETE = 'ROLE_ENTITY_API_ACCOUNT_DELETE';
    static ROLE_ENTITY_API_ACCOUNT_TOTAL = 'ROLE_ENTITY_API_ACCOUNT_TOTAL';

    static ROLE_ENTITY_API_CONSULTING_COMPANY_CRUD = 'ROLE_ENTITY_API_CONSULTING_COMPANY_CRUD';
    static ROLE_CONSULTING_COMPANY_CREATE = 'ROLE_CONSULTING_COMPANY_CREATE';
    static ROLE_ENTITY_API_CONSULTING_COMPANY_GET = 'ROLE_ENTITY_API_CONSULTING_COMPANY_GET';
    static ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE = 'ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE';
    static ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE = 'ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE';
    static ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL = 'ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL';

    static ROLE_ENTITY_API_PERSONAL_AREA_CRUD = 'ROLE_ENTITY_API_PERSONAL_AREA_CRUD';
    static ROLE_PERSONAL_AREA_CREATE = 'ROLE_PERSONAL_AREA_CREATE';
    static ROLE_ENTITY_API_PERSONAL_AREA_GET = 'ROLE_ENTITY_API_PERSONAL_AREA_GET';
    static ROLE_ENTITY_API_PERSONAL_AREA_UPDATE = 'ROLE_ENTITY_API_PERSONAL_AREA_UPDATE';
    static ROLE_ENTITY_API_PERSONAL_AREA_DELETE = 'ROLE_ENTITY_API_PERSONAL_AREA_DELETE';
    static ROLE_PERSONAL_AREA_TOTAL = 'ROLE_PERSONAL_AREA_TOTAL';

    static ROLE_ENTITY_API_SALES_DEPARTMENT_CRUD = 'ROLE_ENTITY_API_SALES_DEPARTMENT_CRUD';
    static ROLE_SALES_DEPARTMENT_CREATE = 'ROLE_SALES_DEPARTMENT_CREATE';
    static ROLE_ENTITY_API_SALES_DEPARTMENT_GET = 'ROLE_ENTITY_API_SALES_DEPARTMENT_GET';
    static ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE = 'ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE';
    static ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE = 'ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE';
    static ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL = 'ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL';

    static ROLE_ENTITY_API_SB_CHECK_STATUS_CRUD = 'ROLE_ENTITY_API_SB_CHECK_STATUS_CRUD';
    static ROLE_SB_CHECK_STATUS_CREATE = 'ROLE_SB_CHECK_STATUS_CREATE';
    static ROLE_ENTITY_API_SB_CHECK_STATUS_GET = 'ROLE_ENTITY_API_SB_CHECK_STATUS_GET';
    static ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE = 'ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE';
    static ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE = 'ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE';
    static ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL = 'ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL';

    static ROLE_ENTITY_API_STATUS_VIP_CRUD = 'ROLE_ENTITY_API_STATUS_VIP_CRUD';
    static ROLE_STATUS_VIP_CREATE = 'ROLE_STATUS_VIP_CREATE';
    static ROLE_ENTITY_API_STATUS_VIP_GET = 'ROLE_ENTITY_API_STATUS_VIP_GET';
    static ROLE_ENTITY_API_STATUS_VIP_UPDATE = 'ROLE_ENTITY_API_STATUS_VIP_UPDATE';
    static ROLE_ENTITY_API_STATUS_VIP_DELETE = 'ROLE_ENTITY_API_STATUS_VIP_DELETE';
    static ROLE_ENTITY_API_STATUS_VIP_TOTAL = 'ROLE_ENTITY_API_STATUS_VIP_TOTAL';

    static ROLE_ENTITY_API_TRADING_PLATFORM_CRUD = 'ROLE_ENTITY_API_TRADING_PLATFORM_CRUD';
    static ROLE_TRADING_PLATFORM_CREATE = 'ROLE_TRADING_PLATFORM_CREATE';
    static ROLE_ENTITY_API_TRADING_PLATFORM_GET = 'ROLE_ENTITY_API_TRADING_PLATFORM_GET';
    static ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE = 'ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE';
    static ROLE_ENTITY_API_TRADING_PLATFORM_DELETE = 'ROLE_ENTITY_API_TRADING_PLATFORM_DELETE';
    static ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL = 'ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL';

    static ROLE_ENTITY_API_TRANSACTION_CRUD = 'ROLE_ENTITY_API_TRANSACTION_CRUD';
    static ROLE_TRANSACTION_CREATE = 'ROLE_TRANSACTION_CREATE';
    static ROLE_ENTITY_API_TRANSACTION_GET = 'ROLE_ENTITY_API_TRANSACTION_GET';
    static ROLE_ENTITY_API_TRANSACTION_UPDATE = 'ROLE_ENTITY_API_TRANSACTION_UPDATE';
    static ROLE_ENTITY_API_TRANSACTION_DELETE = 'ROLE_ENTITY_API_TRANSACTION_DELETE';
    static ROLE_ENTITY_API_TRANSACTION_TOTAL = 'ROLE_ENTITY_API_TRANSACTION_TOTAL';

    static ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL = 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL';
    static ROLE_TYPE_TRADING_ACCOUNT_CREATE = 'ROLE_TYPE_TRADING_ACCOUNT_CREATE';
    static ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET = 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET';
    static ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE = 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE';
    static ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_DELETE = 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_DELETE';
    static ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL = 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL';

    static ROLE_ENTITY_API_USER_CRUD = 'ROLE_ENTITY_API_USER_CRUD';
    static ROLE_USER_CREATE = 'ROLE_USER_CREATE';
    static ROLE_ENTITY_API_USER_GET = 'ROLE_ENTITY_API_USER_GET';
    static ROLE_ENTITY_API_USER_UPDATE = 'ROLE_ENTITY_API_USER_UPDATE';
    static ROLE_ENTITY_API_USER_DELETE = 'ROLE_ENTITY_API_USER_DELETE';
    static ROLE_ENTITY_API_USER_TOTAL = 'ROLE_ENTITY_API_USER_TOTAL';

    static hierarchy = {
        ROLE_ROOT: [
            this.ROLE_SUPER_ADMIN,
        ],

        ROLE_SUPER_ADMIN: [
            this.ROLE_ADMIN,
        ],

        ROLE_ADMIN: [
            this.ROLE_ENTITY_API_BROKER_CRUD,
            this.ROLE_ENTITY_API_ACCOUNT_CRUD,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_CRUD,
            this.ROLE_ENTITY_API_PERSONAL_AREA_CRUD,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_CRUD,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_CRUD,
            this.ROLE_ENTITY_API_STATUS_VIP_CRUD,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_CRUD,
            this.ROLE_ENTITY_API_TRANSACTION_CRUD,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL,
        ],

        ROLE_OFFICE_SUPPORT: [
            this.ROLE_BROKER_CREATE,
            this.ROLE_ENTITY_API_BROKER_GET,
            this.ROLE_ENTITY_API_BROKER_UPDATE,
            this.ROLE_ACCOUNT_CREATE,
            this.ROLE_ENTITY_API_ACCOUNT_GET,
            this.ROLE_ENTITY_API_ACCOUNT_UPDATE,
            this.ROLE_CONSULTING_COMPANY_CREATE,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_GET,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE,
            this.ROLE_PERSONAL_AREA_CREATE,
            this.ROLE_ENTITY_API_PERSONAL_AREA_GET,
            this.ROLE_ENTITY_API_PERSONAL_AREA_UPDATE,
            this.ROLE_SALES_DEPARTMENT_CREATE,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_GET,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE,
            this.ROLE_SB_CHECK_STATUS_CREATE,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_GET,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE,
            this.ROLE_STATUS_VIP_CREATE,
            this.ROLE_ENTITY_API_STATUS_VIP_GET,
            this.ROLE_ENTITY_API_STATUS_VIP_UPDATE,
            this.ROLE_TRADING_PLATFORM_CREATE,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_GET,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE,
            this.ROLE_TYPE_TRADING_ACCOUNT_CREATE,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE,
        ],

        ROLE_SUPPORT_HALF: [
            this.ROLE_ENTITY_API_BROKER_GET,
            this.ROLE_ENTITY_API_ACCOUNT_GET,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_GET,
            this.ROLE_ENTITY_API_PERSONAL_AREA_GET,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_GET,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_GET,
            this.ROLE_ENTITY_API_STATUS_VIP_GET,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_GET,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET,
        ],

        //////////////////////////
        ///// ENTITY ROLES //////
        ////////////////////////
        ROLE_ENTITY_API_BROKER_CRUD: [
            this.ROLE_BROKER_CREATE,
            this.ROLE_ENTITY_API_BROKER_GET,
            this.ROLE_ENTITY_API_BROKER_UPDATE,
            this.ROLE_ENTITY_API_BROKER_DELETE,
            this.ROLE_ENTITY_API_BROKER_TOTAL,
        ],

        ROLE_ENTITY_API_ACCOUNT_CRUD: [
            this.ROLE_ACCOUNT_CREATE,
            this.ROLE_ENTITY_API_ACCOUNT_GET,
            this.ROLE_ENTITY_API_ACCOUNT_UPDATE,
            this.ROLE_ENTITY_API_ACCOUNT_DELETE,
            this.ROLE_ENTITY_API_ACCOUNT_TOTAL,
        ],

        ROLE_ENTITY_API_CONSULTING_COMPANY_CRUD: [
            this.ROLE_CONSULTING_COMPANY_CREATE,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_GET,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE,
            this.ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL,
        ],

        ROLE_ENTITY_API_PERSONAL_AREA_CRUD: [
            this.ROLE_PERSONAL_AREA_CREATE,
            this.ROLE_ENTITY_API_PERSONAL_AREA_GET,
            this.ROLE_ENTITY_API_PERSONAL_AREA_UPDATE,
            this.ROLE_ENTITY_API_PERSONAL_AREA_DELETE,
            this.ROLE_PERSONAL_AREA_TOTAL,
        ],

        ROLE_ENTITY_API_SALES_DEPARTMENT_CRUD: [
            this.ROLE_SALES_DEPARTMENT_CREATE,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_GET,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE,
            this.ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL,
        ],

        ROLE_ENTITY_API_SB_CHECK_STATUS_CRUD: [
            this.ROLE_SB_CHECK_STATUS_CREATE,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_GET,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE,
            this.ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL,
        ],

        ROLE_ENTITY_API_STATUS_VIP_CRUD: [
            this.ROLE_STATUS_VIP_CREATE,
            this.ROLE_ENTITY_API_STATUS_VIP_GET,
            this.ROLE_ENTITY_API_STATUS_VIP_UPDATE,
            this.ROLE_ENTITY_API_STATUS_VIP_DELETE,
            this.ROLE_ENTITY_API_STATUS_VIP_TOTAL,
        ],

        ROLE_ENTITY_API_TRADING_PLATFORM_CRUD: [
            this.ROLE_TRADING_PLATFORM_CREATE,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_GET,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_DELETE,
            this.ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL,
        ],

        ROLE_ENTITY_API_TRANSACTION_CRUD: [
            this.ROLE_TRANSACTION_CREATE,
            this.ROLE_ENTITY_API_TRANSACTION_GET,
            this.ROLE_ENTITY_API_TRANSACTION_UPDATE,
            this.ROLE_ENTITY_API_TRANSACTION_DELETE,
            this.ROLE_ENTITY_API_TRANSACTION_TOTAL,
        ],

        ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL: [
            this.ROLE_TYPE_TRADING_ACCOUNT_CREATE,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_DELETE,
            this.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL,
        ],

        ROLE_ENTITY_API_USER_CRUD: [
            this.ROLE_USER_CREATE,
            this.ROLE_ENTITY_API_USER_GET,
            this.ROLE_ENTITY_API_USER_UPDATE,
            this.ROLE_ENTITY_API_USER_DELETE,
            this.ROLE_ENTITY_API_USER_TOTAL,
        ],
    };

    /**
     * Содержит ли роль в своей иерархии другую роль
     * @param {string} role
     * @param {string} seekRole
     * @return {boolean}
     */
    static roleContainsRole (role, seekRole) {
        console.log('========================');
        console.log('Зашли в roleContainsRole.', role, seekRole);
        let isContains = false;

        if (role === seekRole) {
            console.log('Роли одинаковы, поиск не требуется!');
            return true;
        }

        let roleHierarchyList = this.hierarchy[role];
        if (roleHierarchyList && !roleHierarchyList.length) {
            return false;
        }

        // Если роль конечная и не имеет вложенных ролей, то тогда выходим
        if (roleHierarchyList === undefined) {
            return false;
        }

        console.log(roleHierarchyList); // FIXME: delete before deploy!
        for (let roleHierarchy of roleHierarchyList) {
            console.log('Зашли в roleContainsRole-->for. roleHierarchy:', roleHierarchy);
            if (roleHierarchy === seekRole) {
                return true;
            }
            isContains = isContains || this.roleContainsRole(roleHierarchy, seekRole);

            if (isContains) {
                console.log('====>>>> СОДЕРЖИТ!', role, seekRole); // FIXME: delete before deploy!
                return true;
            }
        }

        console.log('Вышли из roleContainsRole-->for. ', role, seekRole);
        console.log('Вышли из roleContainsRole.', role, seekRole);
        return false;
    }

    /**
     * Получить список содержащихся ролей
     * @param {string} role
     * @param roleList
     * @return {Array<string>}
     */
    static getContainsRoles(role, roleList = []) {
        console.log('Зашли в getContainsRoles(role, roleList)', role, roleList); // FIXME: delete before deploy!
        console.log(`this.hierarchy.hasOwnProperty(${role}):`, this.hierarchy.hasOwnProperty(role), role); // FIXME: delete before deploy!

        let newRoleList = [];
        let intermediateRoleList = this._getContainsRolesByRole(role);

        for(let r of intermediateRoleList) {
            if (this.hierarchy.hasOwnProperty(r)) {
                newRoleList = this._getContainsRolesByRole(r);
            }
        }

        return newRoleList;

        // if (this.hierarchy.hasOwnProperty(role)) {
        //     let newRoleList = Array.prototype.concat(roleList, this.hierarchy[role]);
        //     console.log('Заходим в цикл'); // FIXME: delete before deploy!
        //     for (let r of newRoleList) {
        //         console.log('Вызываем рекурсию', r, newRoleList); // FIXME: delete before deploy!
        //         roleList = this.getContainsRoles(r, roleList);
        //         debugger; // FIXME: delete before deploy!
        //     }
        //     console.log('Вышли из цикла'); // FIXME: delete before deploy!
        // } else {
        //     console.log('Пушим роль: ', role); // FIXME: delete before deploy!
        //     roleList.push(role);
        // }
        //
        // console.log('Возвращаем список ролей: ', roleList); // FIXME: delete before deploy!
        // return roleList;
    }

    static _getContainsRolesByRole(role) {
        let result = [role];
        if (this.hierarchy.hasOwnProperty(role)) {
            for (let r of this.hierarchy[role]) {
                debugger; // FIXME: delete before deploy!
                result.push(r);
            }
        }

        return result;
    }
}

export default RoleHierarchy;
