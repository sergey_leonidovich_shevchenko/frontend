'use strict';

import RoleHierarchy from './RoleHierarhy';
import Identity from '@/components/Security/Identity/Identity.type';

/**
 * Класс отвечающий за доступы к определенным действиям основанными на ролях
 */
class CheckGranted {
    /**
     * Иерархия ролей
     * @type {RoleHierarchy|null}
     * @private
     */
    _roleHierarchy = null;

    /**
     * @return {RoleHierarchy}
     */
    get roleHierarchy() {
        return this._roleHierarchy;
    }

    /**
     * @constructor
     */
    constructor() {
        this._roleHierarchy = new RoleHierarchy();
    }

    /**
     * Проверить есть ли у текущего пользователя данная роль
     * @param {Identity} identity Объект аутентифицированного пользователя
     * @param {Array<string>} seekRoleList Искомый список ролей
     * @return {boolean}
     */
    can(identity, seekRoleList) {
        if (!(identity instanceof Identity)) {
            throw new Error('Error: Identity is not an instance of a IdentityType class!');
        }
        if (!seekRoleList) {
            return false;
        }
        // Если список ролей для доступа к маршруту пуст, то значит разрешаем его отображение
        if (!seekRoleList.length) {
            return true;
        }

        for (let seekRole of seekRoleList) {
            for (let identityRole of identity.roles) {
                let roleIsContainsRole = this.roleHierarchy.constructor.roleContainsRole(identityRole, seekRole);
                debugger; // FIXME: delete before deploy!
                if (roleIsContainsRole) {
                    return true;
                }
            }
        }

        return false;
    }
}

export default CheckGranted;
