'use strict';

import {routes} from '@/router';

let routeList = {
    '*': '/', // otherwise redirect to home
    'home': '/',
    'logout': '/logout',
};

routes.forEach((route) => routeList[route.name] = route.path);

/**
 * Получить маршрут фронтенда по его названию
 * @param {string} frontendRouteName
 * @returns {string}
 */
const frontendRoutes = (frontendRouteName) => {
    if (typeof routeList[frontendRouteName] === 'undefined') {
        throw new Error('Маршрут "' + frontendRouteName + '" не найден!');
    }

    return routeList[frontendRouteName];
};

export default frontendRoutes;
