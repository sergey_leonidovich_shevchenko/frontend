'use strict';

import configEnv from '@/configEnv';
import RoleHierarchy from '@/components/Security/RoleHierarhy';

// Сортировка ключей по алфавиту!
// Названия ключей разделять точкой (api.pages.broker).
// Бывают случаи, что для доступа к одному маршруту, пользователь должен иметь сразу несколько ролей
// (если у пользователя не будет хоть одной роли перечисленных в ключе "roles", то ему доступ будет запрещен)
export const routes = {
    'api.account': {
        path: '/api/account',
        roles: [RoleHierarchy.ROLE_ENTITY_API_ACCOUNT_GET],
    },

    'api.account.total': {
        path: '/api/account/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_ACCOUNT_TOTAL],
    },

    'api.broker': {
        path: '/api/broker',
        roles: [RoleHierarchy.ROLE_ENTITY_API_BROKER_GET],
    },

    'api.broker.total': {
        path: '/api/broker/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_BROKER_TOTAL],
    },

    'api.consulting_company': {
        path: '/api/consulting_company',
        roles: [RoleHierarchy.ROLE_ENTITY_API_CONSULTING_COMPANY_GET],
    },

    'api.consulting_company.total': {
        path: '/api/consulting_company/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL],
    },

    'api.personal_area': {
        path: '/api/personal_area',
        roles: [RoleHierarchy.ROLE_ENTITY_API_PERSONAL_AREA_GET],
    },

    'api.personal_area.total': {
        path: '/api/personal_area/total',
        roles: [RoleHierarchy.ROLE_PERSONAL_AREA_TOTAL],
    },

    'api.sales_department': {
        path: '/api/sales_department',
        roles: [RoleHierarchy.ROLE_ENTITY_API_SALES_DEPARTMENT_GET],
    },

    'api.sales_department.total': {
        path: '/api/sales_department/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL],
    },

    'api.sb_check_status': {
        path: '/api/sb_check_status',
        roles: [RoleHierarchy.ROLE_ENTITY_API_SB_CHECK_STATUS_GET],
    },

    'api.sb_check_status.total': {
        path: '/api/sb_check_status/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL],
    },

    'api.status_vip': {
        path: '/api/status_vip',
        roles: [RoleHierarchy.ROLE_ENTITY_API_STATUS_VIP_GET],
    },

    'api.status_vip.total': {
        path: '/api/status_vip/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_STATUS_VIP_TOTAL],
    },

    'api.trading_platform': {
        path: '/api/trading_platform',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TRADING_PLATFORM_GET],
    },

    'api.trading_platform.total': {
        path: '/api/trading_platform/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL],
    },

    'api.transaction': {
        path: '/api/transaction',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TRANSACTION_GET],
    },

    'api.transaction.total': {
        path: '/api/transaction/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TRANSACTION_TOTAL],
    },

    'api.type_trading_account': {
        path: '/api/type_trading_account',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET],
    },

    'api.type_trading_account.total': {
        path: '/api/type_trading_account/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL],
    },

    'api.user': {
        path: '/api/user',
        roles: [RoleHierarchy.ROLE_ENTITY_API_USER_GET],
    },

    'api.user.total': {
        path: '/api/user/total',
        roles: [RoleHierarchy.ROLE_ENTITY_API_USER_TOTAL],
    },

    'security.login': {
        path: '/security/login',
        roles: ['*'],
    },

    'security.register': {
        path: '/security/register',
        roles: ['*'],
    },
};

/**
 * Получить маршрут бэкенда по его названию
 * @param routeName
 * @returns {string}
 */
const backendRoutes = (routeName) => {
    if (typeof routes[routeName] === 'undefined') {
        throw new Error('Маршрут "' + routeName + '" не найден!');
    }
    return {
        url: configEnv('BACKEND_URL') + routes[routeName].path,
        roles: configEnv('BACKEND_URL') + routes[routeName].roles,
    };
};

export default backendRoutes;
