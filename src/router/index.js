'use strict';

/**
 * Маршрутизатор vue определяет все маршруты для приложения и содержит функцию,
 * которая запускается перед каждым изменением маршрута,
 * чтобы предотвратить доступ неаутентифицированных пользователей к ограниченным маршрутам.
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/content/Home';
import Login from '@/components/content/Login/Login';
import Register from '@/components/content/Register';
import Entity from '@/components/content/pages/Entity';

import Account from '@/components/content/pages/Account';
import Broker from '@/components/content/pages/Broker/Broker';
import ConsultingCompany from '@/components/content/pages/ConsultingCompany';
import PersonalArea from '@/components/content/pages/PersonalArea';
import SalesDepartment from '@/components/content/pages/SalesDepartment';
import SbCheckStatus from '@/components/content/pages/SbCheckStatus';
import StatusVip from '@/components/content/pages/StatusVip';
import TradingPlatform from '@/components/content/pages/TradingPlatform';
import Transaction from '@/components/content/pages/Transaction';
import TypeTradingAccount from '@/components/content/pages/TypeTradingAccount';
import User from '@/components/content/pages/User';

Vue.use(VueRouter);

export const routes = [
  {
    name: 'home',
    path: '/',
    component: Home,
  },
  {
    name: 'login',
    path: '/security/login',
    component: Login,
  },
  {
    name: 'register',
    path: '/security/register',
    component: Register,
  },
  {
    name: 'api.pages',
    path: '/api',
    component: Entity,
  },
  {
    name: 'api.account',
    path: '/api/account',
    component: Account,
  },
  {
    name: 'api.broker',
    path: '/api/broker',
    component: Broker,
  },
  {
    name: 'api.consulting_company',
    path: '/api/consulting_company',
    component: ConsultingCompany,
  },
  {
    name: 'api.personal_area',
    path: '/api/personal_area',
    component: PersonalArea,
  },
  {
    name: 'api.sales_department',
    path: '/api/sales_department',
    component: SalesDepartment,
  },
  {
    name: 'api.sb_check_status',
    path: '/api/sb_check_status',
    component: SbCheckStatus,
  },
  {
    name: 'api.status_vip',
    path: '/api/status_vip',
    component: StatusVip,
  },
  {
    name: 'api.trading_platform',
    path: '/api/trading_platform',
    component: TradingPlatform,
  },
  {
    name: 'api.transaction',
    path: '/api/transaction',
    component: Transaction,
  },
  {
    name: 'api.type_trading_account',
    path: '/api/type_trading_account',
    component: TypeTradingAccount,
  },
  {
    name: 'api.user',
    path: '/api/user',
    component: User,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// router.beforeEach((to, from, next) => {
//   // redirect to login page if not logged in and trying to access a restricted page
//   const publicPages = [
//     frontendRoutes('home'),
//     frontendRoutes('login'),
//     frontendRoutes('register'),
//   ];
//   const authRequired = !publicPages.includes(to.path);
//   const isAuth = localStorage.getItem('authentication-token');
//
//   if (authRequired && !isAuth) {
//     return next(frontendRoutes('login'));
//   }
//
//   next();
// });

export default router;
