/**
 * Выбрать значения у списка объектов(obj) по ключу(columnName)
 * @param {{}[]} objList Список объектов
 * @param {string} columnName Название ключа
 * @returns *[]
 */
export function objectColumn(objList, columnName) {
    let valueList = [];

    objList.forEach(obj => {
        if (typeof obj[columnName] !== 'undefined') {
            valueList.push(obj[columnName]);
        }
    });

    return valueList;
}
