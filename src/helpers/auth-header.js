/**
 * Заголовок Auth - это вспомогательная функция, которая возвращает заголовок HTTP-авторизации,
 * содержащий веб-токен JSON (JWT) текущего пользователя, вошедшего в систему, из локального хранилища.
 * Если пользователь не вошел в систему, возвращается пустой объект.
 * Заголовок authentication используется для выполнения аутентифицированных
 * HTTP-запросов к серверу API с использованием JWT-аутентификации.
 * @returns {{Authorization: string}|{}}
 */
export function authHeader() {
    // return authorization header with jwt token
    let token = JSON.parse(localStorage.getItem('authentication-token'));

    if (token) {
        return {'Authorization': 'Bearer ' + token};
    } else {
        return {};
    }
}
