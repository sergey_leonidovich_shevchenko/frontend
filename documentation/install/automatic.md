Автоматическая установка проекта
================================

#### Для автоматического разворачивания проекта нужно выполнить следующие шаги:

1) Развернуть api backend
2) Зайти в корень текущего проекта
3) Задать следующие переменные окружения (значения можно задать свои):

    |   `Переменная окружения`          |               `Значение`                |                  `Назначение`                   |
    | :-------------------------------- | :-------------------------------------- | :---------------------------------------------- |
    | **IBS_BACKEND_URL**               | ibs-nginx                               | URL бэкенда                                     |
    | **IBS_FRONTEND_APPLICATION_PATH** | /var/www/ibs/frontend                   | Путь к проекту внутри контейнера                |
    | **IBS_FRONTEND_ENV**              | `development`, `production` или `tests` | Среда развертывания                             |
    | **IBS_FRONTEND_HOST_PORT**        | 8080                                    | Порт хостовой машины для подключения к frontend |
    | **IBS_FRONTEND_NETWORK_SUBNET**   | 192.168.220                             | IP подсети контейнеров                          |

4) Запустить команду: `docker-compose up -d --build`
5) После нужно убедиться, что все прошло хорошо, а именно выполнив следующие команды:
    - `docker network ls --filter name=ibs` - мы должны увидеть одну нашу сеть

        |  NETWORK ID  |     NAME    | DRIVER | SCOPE |
        | :----------- | :---------- | :----- | :---- |
        | cd402f354d90 | ibs-network | bridge | local |

    - `docker volume ls --filter name=ibs` - мы должны увидеть один наш volume

        | DRIVER |  VOLUME NAME   |
        | :----- | :------------- |
        | local  | ibs-mysql-data |

    - `docker container ls --filter name=ibs` - мы должны увидеть четыре наших запущенных контейнера (1 от frontend + 3 от backend)
    
        | CONTAINER ID |        IMAGE          |        COMMAND         |      CREATED      |      STATUS      | PORTS                                    |    NAMES     |
        | :----------- | :-------------------- | :--------------------- | :---------------- | :--------------- | :--------------------------------------- | :----------- |
        | a0a64873e254 | frontend_ibs-frontend | "/entrypoint.sh yarn…" | 1 minute ago      | 1 minute ago     | 0.0.0.0:8080->80/tcp                     | ibs-frontend | 
        | 1208105e8ed9 | backend_ibs-nginx     | "nginx -g 'daemon of…" | 2 minute ago      | 2 minute ago     | 0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp | ibs-nginx    |
        | f7799d2b476e | backend_ibs-php-fpm   | "/entrypoint.sh php-…" | 2 minute ago      | 2 minute ago     | 9000/tcp                                 | ibs-php-fpm  |
        | e403fdc8a0e1 | backend_ibs-mysql     | "docker-entrypoint.s…" | 2 minute ago      | 2 minute ago     | 33060/tcp, 0.0.0.0:33060->3306/tcp       | ibs-mysql    |

6) Укажите в Вашей IDE в настройках путь до webpack.config.js
    - ![phpstorm-webpack-config](../settings/phpstorm-webpack-config.png)

7) Теперь можно посмотреть на web-морду по адресам:
    - localhost:`<порт из переменной окружения IBS_FRONTEND_HOST_PORT>` (Например `localhost:8080`)
    - `<подсеть из переменной окружения IBS_FRONTEND_NETWORK_SUBNET>`.4 (Например `192.168.220.4`)
