#!/bin/bash

#########################################
# Очищает результат установки проекта   #
# Удаляет контейнеры с приложениями ibs #
# (полезен при переустановки проекта)   #
# Запускать в контексте корня проекта!  #
#########################################

set -x

docker stop $(docker container ls -f name=ibs-frontend -aq) 2> /dev/null

docker container rm $(docker container ls -f name=ibs-frontend -aq) 2> /dev/null
docker network rm -f name=ibs-frontend-network

docker container prune -f
docker network prune -f

docker image rm -f $(docker images "*ibs-frontend" -q) 2> /dev/null

docker container ls -f name=ibs-frontend
docker network ls -f name=ibs-frontend-network
docker images "*ibs-frontend*"
