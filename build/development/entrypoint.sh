#!/bin/bash

set -e

function addLogMessage {
    echo -e "\n>>>>>>>>>>>> $1 <<<<<<<<<<<<\n"
    echo "[$(date)] $1" >> ./var/projectSetupLog
}

# Если проект не установлен, то устанавливаем его
if [[ ! -f ./var/projectIsInstalled ]]; then
    addLogMessage "Начало установки проекта..."

    addLogMessage 'Создание и наполнение переменных окружений в проект...'
    cp ./.env.example ./.env.local && chmod 777 ./.env.local
    sed -i "s/^VUE_APP_ENV_FILENAME_LOADED=$/VUE_APP_ENV_FILENAME_LOADED=.env.local.local/" ./.env.local
    sed -i "s/^VUE_APP_ENV=$/VUE_APP_ENV=${ENV}/" ./.env.local
    sed -i "s,^VUE_APP_BACKEND_URL=$,VUE_APP_BACKEND_URL=${BACKEND_URL}," ./.env.local
    sed -i "s/^VUE_APP_SNACKBAR_TIMEOUT=$/VUE_APP_SNACKBAR_TIMEOUT=3000/" ./.env.local
    sed -i "s/^VUE_APP_TABLE_ENTITY_PER_PAGE=$/VUE_APP_TABLE_ENTITY_PER_PAGE=25/" ./.env.local
    sed -i "s/^VUE_APP_TABLE_ENTITY_BROKER_PER_PAGE=$/VUE_APP_TABLE_ENTITY_BROKER_PER_PAGE=25/" ./.env.local

    touch ./var/projectIsInstalled && chmod 777 ./var/projectIsInstalled
    echo "[$(date)] The project has been successfully installed!" > ./var/projectIsInstalled
fi

addLogMessage 'Подтягиваем зависимости...'
yarn install

addLogMessage 'Запускаем yarn serve...'
exec "${@}"
