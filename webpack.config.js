const path = require('path');
const Dotenv = require('dotenv-webpack');
const resolve = dirs => path.join(__dirname, ...dirs);


module.exports = {
    debug: true,
    devtool: 'eval-source-map',
    devServer: {
        // proxy: 'http://localhost:8080/',
        contentBase: resolve(process.env.PUBLIC_PATH || '../dist'),
        compress: true,
        historyApiFallback: true,
        host: 'localhost',
        port: 8080,
        open: 'firefox',
        proxy: {
            '**': {
                target: 'localhost:8080',
                secure: false,
            },
        },
        headers: {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Max-Age': '86400',
            'Access-Control-Max-Age2': '86402',
            'Access-Control-Request-Headers': '*',
        },
    },
    output: {
        filename: '[name].[hash].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new Dotenv({
            path: '.env.development',   // load one of the following files depending on the environment variable '.env.development, .env.production, .env.tests'
            safe: true,                 // load '.env.example' to verify the '.env.[mode]' variables are all set. Can also be a string to a different file.
            systemvars: true,           // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
            silent: true,               // hide any errors
            defaults: true,             // load '.env.defaults' as the default values if empty.
        }),
    ],
    resolve: {
        // modules: ['node_modules'],
        extensions: ['.js', '.vue'],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
            '@': resolve('src'),
        },
    },
    rules: [
        {
            test: /\.s(c|a)ss$/,
            use: [
                'vue-style-loader',
                'css-loader',
                {
                    loader: 'sass-loader',
                    // Requires sass-loader@^8.0.0
                    options: {
                        implementation: require('sass'),
                        sassOptions: {
                            fiber: require('fibers'),
                            indentedSyntax: true, // optional
                        },
                    },
                },
            ],
        },
    ],
};
