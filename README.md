# Frontend

#### Есть два способа развернуть проект:
1) [**Автоматический** - развернуть проект в контейнерах с помощью **Docker**](documentation/install/automatic.md)
2) [**Ручной** - развернуть проект самому, пошагово](documentation/install/manual.md)
